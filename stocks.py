from bottle import route, run, view

@route('/')
@view('index')
def index():
		return dict()

@route('/ticker/<t:re:^([A-Z0-9]){1,4}([.]{1}[A-Z]{2})?'>) #regex line 
@view('ticker')
def ticker(t):
	return dict(t=t)

run(host='localhost', port=8080, debug=True)



