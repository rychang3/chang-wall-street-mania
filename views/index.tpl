<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Wall Street Mania</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 


<script>
$(document).on("submit", "form", function(e){ 
        event.preventDefault(); 
        var input = document.getElementById("t").value
        window.location = "/ticker/" + input; 
});  

</script>

%include head

</head>
<body><div id="main">
 
<h1> Wall Street Mania </h1>
<p> Please type a ticker symbol below to see its current market price </p>
<form id="form" name="form">
	<label for="t">Search</label>
	<input type="search" name="t" id="t" pattern="^([A-Z0-9]){1,4}([.]{1}[A-Z]{2})?">
	<input type="submit" value="Submit" id="submit">
</form>
 
</div></body>
</html>